﻿using System.IO;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using System.Xml;
using System.ComponentModel;

namespace PhoneBook.Model
{
    class FileEditor
    {
        public void SaveToFile(string path, ObservableCollection<Phone> collection)
        {
            StreamWriter sw = new StreamWriter(path, false, Encoding.Unicode);
            foreach (Phone item in collection)
            {
                sw.WriteLine(item.FIO);
                sw.WriteLine(item.PhoneNumber);
                sw.WriteLine(item.Text);
            }
            sw.Close();
        }


        public ObservableCollection<Phone> OpenFileTxt(string path)
        {
            StreamReader sr = new StreamReader(path);
            ObservableCollection<Phone> collection = new ObservableCollection<Phone>();
            while (!sr.EndOfStream)
            {
                collection.Add(new Phone(sr.ReadLine(), sr.ReadLine(), sr.ReadLine()));
            }
            sr.Close();
            return collection;
        }

        public void SaveToXml(string path, ObservableCollection<Phone> collection)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(ObservableCollection<Phone>));
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                formatter.Serialize(fs, collection);
            }
        }

        public ObservableCollection<Phone> OpenFileXml(string path)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(ObservableCollection<Phone>));
            ObservableCollection<Phone> ph;
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                ph = (ObservableCollection<Phone>)formatter.Deserialize(fs);
            }
            return ph;
        }

        public void BackgroundOpenFileXml(object sender, DoWorkEventArgs e)
        {
            string path = (string)e.Argument;
            BackgroundWorker backgroundWorker = (BackgroundWorker)sender;
            XmlDocument document = new XmlDocument();
            document.Load(path);
            XmlElement node = document.DocumentElement;
            int Count = node.ChildNodes.Count;
            ObservableCollection<Phone> phones = new ObservableCollection<Phone>();
            Phone ph;
            int i = 0, LastPct = 0, newPct;

            foreach (XmlElement item in node)
            {
                ph = new Phone();
                foreach (XmlNode nod in item)
                {
                    if (nod.Name.Equals("FIO")) { ph.FIO = nod.InnerText; }
                    if (nod.Name.Equals("PhoneNumber")) { ph.PhoneNumber = nod.InnerText; }
                    if (nod.Name.Equals("Text")) { ph.Text = nod.InnerText; }
                }
                phones.Add(ph);
                newPct = (i * 100) / Count;

                if (LastPct != newPct)
                {
                    backgroundWorker.ReportProgress((i * 100) / Count);
                    LastPct = newPct;
                }
                i++;
                //System.Threading.Thread.Sleep(10); Без задержки движение прогрессбара видно на 500 000 - 1 000 000 контактов
            }
            e.Result = phones;
        }


    }
}
