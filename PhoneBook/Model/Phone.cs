﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Model
{
    [Serializable]
    public class Phone
    {
        public string FIO { get; set; }
        public string PhoneNumber { get; set; }
        public string Text { get; set; }

        public Phone()
        {

        }
        public Phone(string fio, string number, string text = "")
        {
            FIO = fio;
            PhoneNumber = number;
            Text = text;
        }
    }
}
