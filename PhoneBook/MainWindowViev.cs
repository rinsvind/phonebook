﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using PhoneBook.Model;
using Microsoft.Win32;
using System.ComponentModel;

namespace PhoneBook
{
    class MainWindowViev : ModelVievBase
    {
        ObservableCollection<Phone> collection = new ObservableCollection<Phone>();
        public ObservableCollection<Phone> Collection
        {
            get
            {
                return collection;
            }

            set
            {
                collection = value;
                OnPropertyChanged("Collection");
            }
        }
        public FileEditor fileEditor = new FileEditor();
        
        Phone selectPhone;
        public Phone SelectPhone
        {
            get
            {
                return selectPhone;
            }
            set
            {
                selectPhone = value;
                OnPropertyChanged("SelectPhone");
            }
        }

        int progressValue = 0;
        public int ProgressValue
        {
            get { return progressValue; }
            set
            {
                progressValue = value;
                OnPropertyChanged("ProgressValue");
            }
        }


        public MainWindowViev()
        {
#if DEBUG
            collection = new ObservableCollection<Phone>{
                new Phone("Кевин Митник", "+375291963654"),
                new Phone("Терри Пратчетт", "+375201232015"),
                new Phone("Ричард Столлман", "+375291619531")
            };
#endif
        }

        RelayCommand save;  // Как это сделать через ApplicationCommands.Save ?
        public ICommand Save
        {
            get
            {
                if (save == null)
                {
                    save = new RelayCommand(SaveExecute, (obj) => collection.Count != 0);
                }
                return save;
            }
        }

        private void SaveExecute(object obj)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                Filter = "txt files (*.txt)|*.txt|xml files (*.xml)|*.xml"
            };

            if (saveFileDialog.ShowDialog() == true)
            {
                if (saveFileDialog.FileName.Contains(".txt"))
                {
                    fileEditor.SaveToFile(saveFileDialog.FileName, collection);
                }
                else
                if (saveFileDialog.FileName.Contains(".xml"))
                {
                    fileEditor.SaveToXml(saveFileDialog.FileName, collection);
                }
            }
        }

        RelayCommand open;
        public ICommand Open
        {
            get
            {
                if (open == null)
                {
                    open = new RelayCommand(OpenExecute, (obj) => true);
                }

                return open;
            }
        }

        private void OpenExecute(object obj)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = "txt files (*.txt)|*.txt|xml files (*.xml)|*.xml"
            };

            if (openFileDialog.ShowDialog() == true)
            {
                if (openFileDialog.FileName.Contains(".txt"))
                {
                    Collection = fileEditor.OpenFileTxt(openFileDialog.FileName);
                }
                else
                if (openFileDialog.FileName.Contains(".xml"))
                {
                    BackgroundWorker bw = new BackgroundWorker();
                    bw.WorkerReportsProgress = true;
                    bw.DoWork += fileEditor.BackgroundOpenFileXml;
                    bw.ProgressChanged += OpenProgressChanged;
                    bw.RunWorkerCompleted += OpenXmlCompleted;
                    bw.RunWorkerAsync(openFileDialog.FileName);
                }

            }

        }

        private void OpenXmlCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Collection.Clear();
            Collection = (ObservableCollection<Phone>)e.Result;
            ProgressValue = 0;
        }

        private void OpenProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ProgressValue = e.ProgressPercentage;
        }

        RelayCommand delete;
        public ICommand Delete
        {
            get
            {
                if (delete == null)
                {
                    delete = new RelayCommand((obj) => Collection.RemoveAt((int)obj), (obj) => collection.Count != 0 && SelectPhone != null);
                }
                return delete;
            }
        }

        RelayCommand add;
        public ICommand Add
        {
            get
            {
                if (add == null)
                {
                    add = new RelayCommand((obj) => Collection.Add(new Phone("Новый контакт", "")), (obj) => true);
                }
                return add;
            }
        }

        RelayCommand deleteAll;
        public ICommand DeleteAll
        {
            get
            {
                if (deleteAll == null)
                {
                    deleteAll = new RelayCommand((obj) => Collection.Clear(), (obj) => collection.Count != 0);
                }
                return deleteAll;
            }
        }
    }
}
